from queue import Queue
from threading import Thread
from lxml import etree
from random import randint
import multiprocessing
import os
import psutil
import datetime

def generator(write_q):
	i=0
	root = etree.Element('random')
	while(i<1000000):
		i=i+1
		num = etree.Element('num')
		num.text = "%s" % randint(1,99999999)
		root.append(num)
	write_q.put(root)


def reader(read_q):
	i=0
	xml = read_q.get()
	f = open('xml_data.txt', 'w')
	for child in xml:
		f.write(child.text + "\n")
	f.close


def monitor(pid):
	fcpu = open("cpu.log","w")
	fmem = open("mem.log", "w")
	p = psutil.Process(pid)
	alive = True
	while(alive):
		cpu = p.cpu_percent(interval=0.01)
		mem = p.memory_percent()
		if cpu == 0.0:
			alive=False
		else:
			fcpu.write(str(datetime.datetime.now()) + "  :  " + str(cpu)+"\n")
			fmem.write(str(datetime.datetime.now()) + "  :  " + str(round(mem,3))+"\n")
	fcpu.close
	fmem.close



pid = os.getpid()
q = Queue()
t1 = Thread(target=generator, args=(q,))
t2 = Thread(target=reader, args=(q,))
t1.start()
t2.start()
p1 = multiprocessing.Process(target=monitor, args=(pid,))
p1.start()
q.join()